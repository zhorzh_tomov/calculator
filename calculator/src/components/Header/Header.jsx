import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import "./Header.css";

const Header = () => {
  const [city, setCity] = useState("Varna");
  const [form, setForm] = useState({
    estimatedCost: 1000,
    cashOnDelivery: 1000,
    weight: 1000,
    width: 100,
    height: 100,
    length: 100,
  });
  const updateForm = (prop) => (e) => {
    setForm({
      ...form,
      [prop]: e.target.value,
    });
  };
  useEffect(() => {}, []);
  return (
    <div className="header">
      <div className="oaDeliveryCalc">
        <div id="calculator" className="panel panel-default">
          <div className="well" id="locality">
            <p>
              <a
                className="btn btn-primary"
                href="#"
                data-toggle="modal"
                data-target="#oaCitiesModal"
                role="button"
              >
                Choose
              </a>
              <a
                className="btn btn-primary"
                href="#"
                data-toggle="rebuild"
                role="button"
              >
                Recalculate
              </a>
              <a
                className="btn btn-primary"
                href="#"
                data-toggle="modal"
                data-target="#oaServicePointsModal"
                role="button"
              >
                Map
              </a>
            </p>
            <div className="col-lg-5">
              <div className="form-group has-feedback has-feedback-left">
                <input
                  type="text"
                  id="postcode"
                  className="form-control"
                  aria-label="Postcode"
                  placeholder="Postcode"
                  data-target="postcode"
                />
              </div>
            </div>
          </div>
          <div className="row margin-bottom">
            <div className="col-lg-4">
              <label for="estimatedCost">Estimated cost (rub.)</label>
              <input
                type="text"
                name="estimatedCost"
                data-target="settings"
                className="form-control"
                aria-label="Estimated cost (rub.)"
                placeholder={form.estimatedCost}
                onChange={updateForm("estimatedCost")}
              ></input>
            </div>
            <div className="col-lg-4">
              <label for="payment">Cash on delivery (rub.)</label>
              <input
                type="text"
                name="payment"
                data-target="settings"
                className="form-control"
                aria-label="Cash on delivery (rub.)"
                placeholder={form.cashOnDelivery}
                onChange={updateForm("cashOnDelivery")}
              />
            </div>
            <div className="col-lg-4">
              <div className="label-input">
                <label for="weight">Weight (gram)</label>
                <input
                  type="text"
                  name="weight"
                  data-target="settings"
                  className="form-control"
                  aria-label="Weight (gram)"
                  placeholder={form.weight}
                  onChange={updateForm("weight")}
                />
              </div>
            </div>
          </div>
          <div className="row margin-bottom">
            <div className="col-lg-4">
              <label for="width">Width (mm)</label>
              <input
                type="text"
                name="width"
                data-target="settings"
                className="form-control"
                aria-label="Width (mm)"
                placeholder={form.width}
                onChange={updateForm("width")}
              />
            </div>
            <div className="col-lg-4">
              <label for="height">Height (mm)</label>
              <input
                type="text"
                name="height"
                data-target="settings"
                className="form-control"
                aria-label="Height (mm)"
                placeholder={form.height}
                onChange={updateForm("height")}
              />
            </div>
            <div className="col-lg-4">
              <div className="label-input">
                <label for="length">Length (mm)</label>
                <input
                  type="text"
                  name="length"
                  data-target="settings"
                  className="form-control"
                  aria-label="Length (mm)"
                  placeholder={form.length}
                  onChange={updateForm("length")}
                />
              </div>
            </div>
          </div>
          <div className="col-lg-12">
            <h4>Delivery Services</h4>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Header;
