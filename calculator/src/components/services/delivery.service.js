import { ref, get, query, equalTo, orderByChild, update, onValue , set} from 'firebase/database';
import { db } from '../config/firebase-config';


export const fromLocationsDocument = snapshot => {
    const locationsDocument = snapshot.val();
  
    return Object.keys(locationsDocument).map(key => {
      const location = locationsDocument[key];
  
      return {
        ...location,
      };
    });
  };
export const getAllLocations = async () => {
    return await get(ref(db, 'locations'))
    .then(snapshot => {
        if(!snapshot.exists()) {
            return [];
        }
        return fromLocationsDocument(snapshot)

    })
}
export const getAllLocationsByCity = async (city) => {
    return await get(ref(db, `locations/${city}`))
        .then(result => {
            const location = result.val();
            return location
        })
}
