import './App.css';
import { BrowserRouter, Route} from "react-router-dom";
import Header from './components/Header/Header';
import Recalculate from './components/Recalculate/Recalculate';

function App() {
  return (
    <BrowserRouter>
    <div className="App">
      <Header/>
      <div className='Outlet'>
      
      </div>
    </div>
    </BrowserRouter>
  );
}

export default App;
